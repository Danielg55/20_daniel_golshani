﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Ex20
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string myCurrentCard;
        bool myCardIsFlipped = false;
        NetworkStream clientStream;      

        public void resetCards()
        {
            foreach (Control x in this.Controls)
            {
                if (x is PictureBox)
                {
                    ((PictureBox)x).Image = Image.FromFile("PNG-cards/card back red.png");
                }
            }
            pictureBox11.Image = Image.FromFile("PNG-cards/card back blue.png");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            TcpClient tcpClientC = new TcpClient();

            Int32 port = 8820;
            IPAddress ip = IPAddress.Parse("127.0.0.1");

            tcpClientC.Connect(ip, port);

            clientStream = tcpClientC.GetStream();

            Thread t = new Thread(() => listen(clientStream));
            t.Start();

            resetCards();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            ChangeEnabled(false, true);
            string[] dir = Directory.GetFiles("PNG-cards/Cards");
            int randomNumber = new Random().Next(0,52);
            (sender as PictureBox).Image = Image.FromFile(dir[randomNumber]);
            string parsedCardName = parseCard(dir[randomNumber]);

            myCurrentCard = parsedCardName;
            myCardIsFlipped = true;
            
            Send("1"+parsedCardName);
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }

        public void Send(string message)
        {
            byte[] buffer = new ASCIIEncoding().GetBytes(message);
            clientStream.Write(buffer, 0, 4);
            clientStream.Flush();
        }

        static string Recieve(NetworkStream clientStream)
        {
            byte[] bufferIn = new byte[4];
            int bytesRead = clientStream.Read(bufferIn, 0, 4);
            return new ASCIIEncoding().GetString(bufferIn);
        }
        public void listen(NetworkStream clientStream)
        {
            ChangeEnabled(false, false);
            string message;

            while (true)
            {
                string cardImage = "PNG-cards", opCurrentCard;
                message = Recieve(clientStream);
                if (message[0] == '0') //game starts
                {
                    ChangeEnabled(true, false);
                }
                else if (message[0] == '1')
                {
                    opCurrentCard = message.Substring(1, 3);

                    foreach (string path in Directory.GetFiles("PNG-cards/Cards")) //getting card image
                    {
                        if (parseCard(path) == opCurrentCard)
                        {
                            cardImage = path;
                        }
                    }

                    while (true) //waiting for player to select his card
                    {
                        if (myCardIsFlipped)
                        {
                            pictureBox11.Image = Image.FromFile(cardImage);
                            int myCard = Int32.Parse(myCurrentCard.Substring(0, 2)), opCard = Int32.Parse(opCurrentCard.Substring(0, 2));
                            
                            if (myCard > opCard)
                            {
                                MyScoreCount.Invoke(new Action(() => MyScoreCount.Text = (Int32.Parse(MyScoreCount.Text)+1).ToString()));
                            }
                            else if (myCard < opCard)
                            {
                                OpScoreCount.Invoke(new Action(() => OpScoreCount.Text = (Int32.Parse(OpScoreCount.Text) + 1).ToString()));
                            }

                            Thread.Sleep(2000);
                            resetCards();
                            ChangeEnabled(true, false);
                            myCardIsFlipped = false;
                            break;
                        }
                    }
                }
                else //opponent quit
                {
                    Application.Exit();
                    break;
                }
            }
        }

        public string parseCard(string cardName)
        {
            string returnVal = "000";
            string[] cardTypes = {"ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "jack", "queen", "king"};
            string[] cardTypesParsed = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13"};

            int i = 0;
            foreach(string a in cardTypes)
            {
                if (cardName.IndexOf(a) >= 0) 
                {
                    returnVal = cardTypesParsed[i];
                }
                i++;
            }

            if (cardName.IndexOf("clubs") >= 0)
            {
                returnVal += "C";
            }
            else if (cardName.IndexOf("diamonds") >= 0)
            {
                returnVal += "D";
            }
            else if (cardName.IndexOf("hearts") >= 0)
            {
                returnVal += "H";
            }
            else
            {
                returnVal += "S";
            }

            return returnVal;
        }

        void ChangeEnabled(bool enabled, bool lockQuitButton)
        {
            foreach (Control c in this.Controls)
            {
                if ((!lockQuitButton || !(c is Button)) && !(c is Label))
                    c.Invoke(new Action (() => c.Enabled = enabled));
            }
        } 

        private void QuitButton_Click(object sender, EventArgs e)
        {
            Send("2000");
            Application.Exit();
        }
    }
}
